<?php function _history()
{?>
<?php include 'config/_configdb.php'?>
<?php include 'app/notification.php'?>
<nav class="breadcrumb"><i class="fa fa-history"></i> <span class="c-666"> ປະຫວັດການເປີດເພງ</span><?php @btn_control()?></nav>
<article class="cl pd-10">
	<div class="col-md-12 box">
	<div class="col-md-12">
	<table class="table table-border table-bg" id="sampleTable2">
		<thead>
			<tr>
				<th>#</th>
				<th>ລາຍການເພງ</th>
				<th>ເລກໂຕະ</th>
				<th>ພິເສດ</th>
				<th>ສະຖານະ</th>
				<th>ວັນທີ</th>
				<th>ອາຍຸ</th>
			</tr>
		</thead>
		<tbody>
			<?php
        $i = 1;
    $create = $sql($con, "SELECT  year(curdate())-year(createdAt)as year,month(curdate())-month(createdAt)as month,day(curdate())-day(createdAt)as day FROM qs_customer");
    $yy = $assoc($create);
    $_listSong = $sql($con, "SELECT * FROM qs_order_song order by Id desc");
    while ($_song = $array($_listSong)) {
        ?>
			<tr>
				<td><?php echo $i ?></td>
				<td><i class="fa fa-music"></i> <?php echo $_song['title'] ?></td>
				<td><?php echo $_song['tb_number'] ?></td>
				<td><?php echo $_song['detail'] ?></td>
				<td>
					<i class="fa fa-<?php if ($_song['status'] == 'on') {echo 'pause';} else {echo 'stop';}?>">
					</td>
				<td><?php echo $_song['createdAt'] ?></td>
				<td> 
				<?php echo $yy['year'] ?> ປີ
                <?php echo $yy['month'] ?> ເດືອນ
             <?php echo $yy['day'] ?> ວັນ ຜ່ານມາ</p> 
				</td>
			</tr>
			<?php $i++;}?>
		</tbody>
	</table>
	</div>
</article>
<?php }?>
