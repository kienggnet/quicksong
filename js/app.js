// ອອກຈາກລະບົບ
function logout(e){
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການອອກຈາກລະບົບແທ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='login?/home'+e},1000)
    },function(){});
}

function remUser(e) {
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ແທ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='?/AllMember&/&delUser='+e}, 1000)
    },function(){});
}
function delCompany(e) {
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ແທ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='?/company&/&delCp='+e}, 1000)
    },function(){});
}
function delsong(e) {
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ແທ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='?/addOrder&/&del='+e}, 1000)
    },function(){});
}

function Onplay(evt) {
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການເປີດເພງນີ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='?/home&/&del='+evt}, 1000)
    },function(){});
}
function Block(evt) {
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການບລ໋ອກຜູ້ໃຊ້ນີ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='?/peopleOnline&/&block='+evt}, 1000)
    },function(){});
}
function Remove(evt) {
  Notiflix.Confirm.Show( 
    'ແຈ້ງເຕືອນ',
    'ທ່ານຕ້ອງການລຶບຜູ້ໃຊ້ນີ້ ຫຼື ບໍ່ ?',
    'ຕົກລົງ',
    'ປິດ',function(){
      Notiflix.Loading.Standard('ກຳລັງດຳເນີນງານ...');setInterval(function () {window.location='?/peopleOnline&/&move='+evt}, 1000)
    },function(){});
}

//==============search data
$(document).ready(function () {
	$('#search').keyup(function () {
		data($(this).val());
	});

	function data(value) {
		$('#display #data').each(function () {
			var found = 'false';
			$(this).each(function () {
				if ($(this).text().toLowerCase().Of(value.toLowerCase()) >= 0) {
					found = 'true';
				}
			});
			if (found == 'true') {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	}
});



function print(print_content) {
  var printContents = document.getElementById('print_content').innerHTML;
  var originalContents = document.body.innerHTML;
  document.body.innerHTML = printContents;
  setTimeout(function() {
    this.close();
    locatoin='';
  }, 1000);
  window.print();
  document.body.innerHTML = originalContents;
}

function UpdateSong(e){
   $('#updateSong').modal()
  $.get('addOrder/updateSong.php',{e:e},function(data){
    $('#songContent').html(data)
  })
}

$(document).ready(function(){
       setInterval(function () {
        $('#display').load('LoadData.php')
       }, 1000)
    })

$(document).ready(function(){
       setInterval(function () {
        $('#displays').load('peopleOnline.php');
       }, 1000)
    })

function profile(title,url,w,h,id){
    url='profile.php?id'+id
  if (w == null || w == '') {
    w=600;
  };
  if (h == null || h == '') {
    h=($(window).height() - 30);
  };
  layer.open({
    type: 2,
    area: [w +'px', h +'px'],
    fix: false, 
    maxmin: true,
    shade:0.4,
    title: title,
    content: url
  });
}
function profileStudent(title,url,w,h,id){
    url='profile.php?id'+id
  if (w == null || w == '') {
    w=500;
  };
  if (h == null || h == '') {
    h=($(window).height() - 30);
  };
  layer.open({
    type: 2,
    area: [w +'px', h +'px'],
    fix: false, 
    maxmin: true,
    shade:0.4,
    title: title,
    content: url
  });
}

function profileMember(title,url,w,h,id){
    url='Member/profile_member.php?id='+id
  if (w == null || w == '') {
    w=600;
  };
  if (h == null || h == '') {
    h=($(window).height() - 30);
  };
  layer.open({
    type: 2,
    area: [w +'px', h +'px'],
    fix: false, 
    maxmin: true,
    shade:0.4,
    title: title,
    content: url
  });
}

var today = new  Date();
var h = today.getHours();
var min = today.getMinutes();
var s = today.getSeconds();
var times=(h+':'+min+':'+h)


$('.numbers').keyup(function(){
    if(this.value*1!=this.value)this.value='';
  });

