
<?php function settings(){ ?>
<?php include 'config/_configdb.php' ?>
<?php include 'app/notification.php' ?>
<nav class="breadcrumb"><i class="fa fa-cog"></i> <span class="c-666"> ຕັ້ງຄ່າ</span><?php @btn_control() ?></nav>
<article class="cl pd-20">
	<div <?php echo $permis; ?> class="submenu">
		<a href="?/AllMember&/<?php echo $uri;?>">
			<i class="fa fa-user fa-4x"></i><br>
			<div class="endsub">
				<strong> ຜູ້ໃຊ້ລະບົບ</strong>
			</div>
		</a>
	</div>
	<div <?php echo $permis; ?> class="submenu">
		<a href="#" data-toggle="modal" data-target="#showPermis">
			<i class="fa fa-user-secret fa-4x"></i><br>
			<div class="endsub">
				<strong>ກຳນົດສິດທິ</strong>
			</div>
		</a>
	</div>
	<div class="submenu">
		<a href="?/company&/<?php echo $uri;?>">
			<i class="fa fa-bank fa-4x"></i><br>
			<div class="endsub">
				<strong>ສຳນັກງານ</strong>
			</div>
		</a>
	</div>
	<div class="submenu">
		<a href="#" data-toggle="modal" data-target='#ChangePassword'>
			<i class="fa fa-expeditedssl fa-4x"></i><br>
			<div class="endsub">
				<strong>ປ່ຽນລະຫັດຜ່ານ</strong>
			</div>
		</a>
	</div>
	<div <?php echo $permis; ?> class="submenu">
		<a href="#" data-toggle="modal" data-target='#BlockUser'>
			<i class="fa fa-user-times fa-4x"></i><br>
			<div class="endsub">
				<strong>ບລ໋ອກສະມາຊິກ</strong>
			</div>
		</a>
	</div>
</article>

<!-- Modal -->
<div class="modal fade" id="showPermis" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="#" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel"><strong>ກຳນົດສິດທິ</strong></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label for="">ສະມາຊິກ</label>
					<select name="user_id" class="input-text" required="true" style="width:100%!important">
						<option value="">-- ເລືອກ --</option>
						<?php 
						$_callUser=$sql($con,"SELECT*FROM apm_users where block='true'");
						while($row=$array($_callUser)){ ?>
						<option value="<?php echo $row['user_id'] ?>"><?php echo $row['user_fname'] ?> (<?php echo $row['user_lname'];?>)</option>
						<?php } ?>
					</select>
					<label>ສິດທິການໃຊ້ງານ <?php @val(); ?></label>
					<select name="user_status" class="input-text" id="">
						<option value="">-- ເລືອກ --</option>
						<option value="Admin">Admin</option>
						<option value="Users">Users</option>
					</select><br><br>
				</div>
				<div class="modal-footer">
					<button type="submit" name="Accept" class="btn btn-success"><i class="fa fa-check"></i> ຢືນຢັນ</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> ປິດ</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="BlockUser" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="#" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel"><strong>ບລ໋ອກສະມາຊິກ</strong></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label for="">ສະມາຊິກ</label>
					<select name="userId" class="input-text" style="width:100%!important" required="true">
						<option value="">-- ເລືອກ --</option>
						<?php 
						$_callUser=$sql($con,"SELECT*FROM apm_users");
						while($row=$array($_callUser)){ ?>
						<option value="<?php echo $row['user_id'] ?>"><?php echo $row['user_fname'] ?> (<?php echo $row['user_lname'];?>)</option>
						<?php } ?>
					</select>
					<label>ບລ໋ອກ <?php @val(); ?></label>
					<select name="block" class="input-text">
						<option value="">-- ເລືອກ --</option>
						<option value="false">ບລ໋ອກ</option>
						<option value="true">ປົດບລ໋ອກ</option>
					</select><br><br>
				</div>
				<div class="modal-footer">
					<button type="submit" name="blocked" class="btn btn-success"><i class="fa fa-check"></i> ຢືນຢັນ</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> ປິດ</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="ChangePassword" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
		<form action="#" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel"><strong>ປ່ຽນລະຫັດຜ່ານ</strong></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<center><img src="img/<?php if($account_img){echo $account_img;}else{echo 'user_null.png';}?>" alt="" data-darkbox='img/<?php echo $account_img; ?>' data-darkbox-group='two' id="load"></center><br>
					<label for="">ຊື່</label>
					<input type="text" class="input-text" disabled value="<?php echo $account_fname;?>">
					<label>ລະຫັດເກົ່າ <?php @val(); ?></label>
					<input type="password" class="input-text" name="old_pass" autofocus minlength="6" required placeholder="ກະລຸນາປ້ອນລະຫັດເກົ່າ">
					<label>ລະຫັດໃໝ່ <?php @val(); ?></label>
					<input type="password" class="input-text" name="new_pass" minlength="6" required placeholder="ກະລຸນາປ້ອນລະຫັດໃໝ່">
					<label>ຢືນຢັນລະຫັດໃໝ່ <?php @val(); ?></label>
					<input type="password" class="input-text" name="cfnew_pass" minlength="6" required placeholder="ກະລຸນາຢືນຢັນລະຫັດໃໝ່">
					<br><br>
				</div>
				<div class="modal-footer">
					<button type="submit" name="change" class="btn btn-success"><i class="fa fa-check"></i> ຢືນຢັນ</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> ປິດ</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?php script() ?>
<?php 
	if(isset($_POST['Accept'])){
		$user_id=$_POST['user_id'];
		$user_status=$_POST['user_status'];
		$Accept=$sql($con,"UPDATE apm_users set user_status='$user_status' where user_id='$user_id'");
		if($Accept){echo $Success;}else{echo $Fail;}
	}
	if(isset($_POST['blocked'])){
		$userId=$_POST['userId'];
		$block=$_POST['block'];
		$finished=$sql($con,"UPDATE apm_users set block='$block' where user_id='$userId'");
		if($finished){echo $Success;}else{echo $Fail;}
	}

	if(isset($_POST['change'])){
		$old_pass=md5($_POST['old_pass']);
		$new_pass=md5($_POST['new_pass']);
		$cfnew_pass=md5($_POST['cfnew_pass']);
		if($old_pass!=$account_password){
			echo $not_match;
		}elseif($new_pass!=$cfnew_pass){
			echo $confirm_not_match;
		}else{
			$change_pass=$sql($con,"UPDATE apm_users set user_password='$cfnew_pass' where user_id='$account_id'");
			if($change_pass){echo $Logout;}else{echo $val_none;}
		}
	}
 ?>
<?php } ?>