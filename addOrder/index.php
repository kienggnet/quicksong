<?php function _AddOrders(){ ?>
<?php include 'config/_configdb.php' ?>
<?php include 'app/notification.php' ?>
<nav class="breadcrumb"><i class="fa fa-music"></i> <span class="c-666"> ລາຍການອໍເດີ້ເພງ</span><?php @btn_control() ?></nav>
<article class="cl pd-10">
	<div class="col-md-12 box">
	<div class="col-md-4">
	<h4>ເພີ່ມຂໍ້ມູນອໍເດີ້</h4>
	<hr>
	<form action="#" method="post">
	<label for="">ເລກໂຕະ</label>
	<select name="tb_number" class="input-text" id="">
	<option>ເລືອກເລກໂຕະ</option>
	<?php 
		for($i=1; $i<50;$i++){
		?>
		<option value="<?php echo $i;?>">0<?php echo $i;?></option>
	<?php
		}
	 ?>
	</select>
	<label for="">ຊື່ເພງ</label>
	<input type="text" class="input-text" name="title" required="true" placeholder="ກະລຸນາປ້ອນຊື່ເພງ"><br>
	<label for="">link ເພງ</label>
	<input type="text" class="input-text" name="link" placeholder="ກະລຸນາປ້ອນ link ເພງທີ່ທ່ານມັກຈາກ Youtube"><br>
	<label for="">ພິເສດ</label>
	<textarea class="textarea" name="detail" placeholder="ເປັນພິເສດ..."></textarea>
	<br>
	<br>
	<button type="submit" name="handleClick" class="btn btn-success"><i class="fa fa-save"></i> ບັນທຶກ</button>
	<button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> ຍົກເລີກ</button>
	</form>
	</div>
	<div class="col-md-8">
	<table class="table table-border table-bg" id="sampleTable2">
		<thead>
			<tr>
				<th>#</th>
				<th>ລາຍການເພງ</th>
				<th>ເລກໂຕະ</th>
				<th>ພິເສດ</th>
				<th>ສະຖານະ</th>
				<th>ວັນທີ</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$i=1;
				$_listSong=$sql($con,"SELECT * FROM qs_order_song order by Id desc");
				while($_song=$array($_listSong)){
			 ?>
			<tr>
				<td><?php echo $i ?></td>
				<td><i class="fa fa-music"></i> <?php echo $_song['title'] ?></td>
				<td><?php echo $_song['tb_number'] ?></td>
				<td><?php echo $_song['detail'] ?></td>
				<td>
					<i class="fa fa-<?php if($_song['status']=='on'){echo 'pause';}else{echo 'stop';} ?>">
					</td>
				<td><?php echo $_song['createdAt'] ?></td>
				<td>
					<div class="btn-group right">
						<a <?php echo $permis3; ?> href="#" onclick="UpdateSong('<?php echo $_song[Id] ?>')" class="btn btn-default"><i class="fa fa-pencil"></i></a>
						<a <?php echo $permis3; ?> href="#" class="btn btn-default" onclick="delsong('<?php echo $_song[Id] ?>')"><i class="fa fa-times"></i></a>
					</div>
				</td>
			</tr>
			<?php $i++;} ?>
		</tbody>
	</table>
	</div>
</article>
<?php 
	if(isset($_POST['handleClick'])){
		$title=$Setstring($con,$_POST['title']);
		$link=$Setstring($con,$_POST['link']);
		$tb_number=$Setstring($con,$_POST['tb_number']);
		$detail=$Setstring($con,$_POST['detail']);
		$newData=$sql($con,"INSERT INTO qs_order_song (tb_number,title,detail,status,createdAt,createdBy) values('$tb_number','$title','$link','$detail','on','$timestam','$account_fname')");
		if($newData){echo $Success;}else{echo $Fail;}
	}

	if(isset($_GET['del'])){
		$moveItem=$sql($con,"DELETE FROM qs_order_song where id='$_GET[del]'");
		if($moveItem){echo $success;}else{echo $fail;}
	}
 ?>
 <!-- edit district -->
<div class="modal fade" id="updateSong" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
  	<form action="#" method="post">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">ແກ້ໄຂຂໍ້ມູນອໍເດີ້</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="songContent">
      </div>
      <div class="modal-footer">
        <button type="submit" name="handleSubmit" class="btn btn-success"><i class="fa fa-check"></i> ຢືນຢັນ</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> ປິດ</button>
      </div>
    </div>
	</form>
  </div>
</div>
<?php 
	if(isset($_POST['handleSubmit'])){
		$Id=$Setstring($con,$_POST['Id']);
		$tb_number=$Setstring($con,$_POST['tb_number']);
		$title=$Setstring($con,$_POST['title']);
		$link=$Setstring($con,$_POST['link']);
		$detail=$Setstring($con,$_POST['detail']);
		$newDatas=$sql($con,"UPDATE qs_order_song SET tb_number='$tb_number',title='$title',link='$link',detail='$detail',updatedAt='$timestam',updatedBy='$account_fname' where Id='$Id'");
		if($newDatas){echo $Success;}else{echo $Fail;}
	}
?>
<?php 	} ?>
