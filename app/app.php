
<?php function package(){?>
<?php include 'config/_configdb.php' ?>
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" type="text/css" href="static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/style.css" />
<link rel="stylesheet" href="css/select2.min.css">
<link rel="stylesheet" href="css/app.css">
<link rel="stylesheet" href="font/font-style.css">
<link rel="stylesheet" href="css/component.css">
<link rel="stylesheet" href="AIO/darkbox.css">
<link rel="stylesheet" href="font-awesome/4.5.0/css/font-awesome.min.css">

<style>
*{font-family:LAOS }
</style>
<?php } ?>
<?php function navbar(){ ?>
<?php include 'config/_configdb.php' ?>
<?php include 'Company/setDefault.php' ?>
<header class="navbar-wrapper">
<div class="navbar navbar-fixed-top">
<div class="container-fluid cl">
	<a class="logo navbar-logo f-l mr-10" href="?/home&/<?php echo $uri;?>">
		<img src="img/<?php echo $set_df['logo_image'];?>" alt="" style="width: 35px;height: 35px;">
		<?php echo $set_df['name_l'].' / '.$set_df['name_e']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</a>		
		<a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
		<nav id="Hui-userbar" class="nav navbar-nav navbar-userbar">
			<ul class="cl">
				<li id="Hui-msg"><?php @search() ?></li>
				<li class="dropDown dropDown_hover"> <a href="#" class="dropDown_A"><i class="fa fa-cogs" style="color:white!important"></i> <i class="Hui-iconfont">&#xe6d5;</i></a>
					<ul class="dropDown-menu menu radius box-shadow">
						<li><a href="javascript:;" onClick="profile('ໂປຣໄຟລ໌ຂອງ <?php echo $account_fname.' '.$account_lname;?>')">ໂປຣໄຟລ໌</a></li>
						<li><a href="?/settings&/<?php echo $uri;?>">ຕັ້ງຄ່າ</a></li>
						<li><a href="#" onclick="logout('<?php echo $uri;?>')"> ອອກຈາກລະບົບ</a></li>
					</ul>
				</li>
				<li id="Hui-skin" class="dropDown right dropDown_hover"> <a href="javascript:;" class="dropDown_A" title="ປ່ຽນສີ"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
					<ul class="dropDown-menu menu radius box-shadow">
						<li><a href="javascript:;" data-val="default" title="ສີດຳ">ສີດຳ</a></li>
						<li><a href="javascript:;" data-val="blue" title="ສີຟ້າ">ສີຟ້າ</a></li>
						<li><a href="javascript:;" data-val="green" title="ສີຂຽວ">ສີຂຽວ</a></li>
						<li><a href="javascript:;" data-val="red" title="ສີແດງ">ສີແດງ</a></li>
						<li><a href="javascript:;" data-val="yellow" title="ສີເຫຼືອງ">ສີເຫຼືອງ</a></li>
						<li><a href="javascript:;" data-val="orange" title="ສີສົ້ມ">ສີສົ້ມ</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>
</header>
<?php } ?>

<?php function menu(){ ?>
<?php include 'config/_configdb.php' ?>
<aside class="Hui-aside">
<div class="menu_dropdown bk_2">
	<dl id="menu-addOrders" style="font-size: 16px!important">
		<dt onclick="window.location='?/addOrder&/<?php echo $uri;?>'">
			<i class="fa fa-music"></i> ເພີ່ມອໍເດີ້ເພງ</dt>
	</dl>
	<dl id="menu-active" style="font-size: 16px!important">
		<dt onclick="window.location='?/peopleOnline&/<?php echo $uri;?>'">
			<i class="fa fa-users"></i> ສະມາຊິກ</dt>
	</dl>
	<dl id="menu-history" onclick="window.location='?/history&/<?php echo $uri;?>'" style="font-size: 16px!important">
		<dt><i class="fa fa-history"></i> ປະຫວັດການເປີດເພງ</dt>
	</dl>
</div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<?php } ?>

<?php function script(){ ?>
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.js"></script> 
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.page.js"></script> 
<script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">$('#sampleTable').DataTable();</script>
<script type="text/javascript">$('#sampleTable2').DataTable();</script>
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
<script src="js/onload_file.js"></script>
<script src="js/select2.full.min.js"></script>
<script src="js/custom-file-input.js"></script>
<script src="js/app.js"></script>
<script src="AIO/darkbox.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
function admin_add(title,url,w,h){
	layer_show(title,url,w,h);
}

$('#demoDate').datepicker({
	format: "yyyy/mm/dd",
	autoclose: true,
	todayHighlight: true
});

$('#demoDate2').datepicker({
	format: "yyyy/mm/dd",
	autoclose: true,
	todayHighlight: true
});

$('#demoSelect').select2();
$('#demoSelect4').select2();
$('#demoSelect3').select2();

function admin_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){


		$(obj).parents("tr").remove();
		layer.msg('已删除!',{icon:1,time:1000});
	});
}

function admin_edit(title,url,id,w,h){
	layer_show(title,url,w,h);
}

function admin_stop(obj,id){
	layer.confirm('确认要停用吗？',function(index){

		$(obj).parents("tr").find(".td-manage").prepend('<a onClick="admin_start(this,id)" href="javascript:;" title="启用" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>');
		$(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">已禁用</span>');
		$(obj).remove();
		layer.msg('已停用!',{icon: 5,time:1000});
	});
}

function admin_start(obj,id){
	layer.confirm('确认要启用吗？',function(index){


		$(obj).parents("tr").find(".td-manage").prepend('<a onClick="admin_stop(this,id)" href="javascript:;" title="停用" style="text-decoration:none"><i class="Hui-iconfont">&#xe631;</i></a>');
		$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
		$(obj).remove();
		layer.msg('已启用!', {icon: 6,time:1000});
	});
}
$('.table-sort').dataTable();

$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
})

</script>
<?php } ?>