<?php function _Editcompany(){ ?>
<?php include 'config/_configdb.php' ?>
<?php include 'app/notification.php' ?>
<?php 
	$callDataStatus=$sql($con,"SELECT count(id) from qs_company where status='on'");
	$datas=$array($callDataStatus);
	$data=$datas[0];

	$GetData=$sql($con,"SELECT*FROM qs_company where id='$_GET[id]'");
	$call=$assoc($GetData);
 ?>
<nav class="breadcrumb"><i class="fa fa-bank"></i> <span class="c-666"><a href="?/company&/<?php echo $uri;?>">ຂໍ້ມູນສຳນັກງານ</a> / ແກ້ໄຂ</span><?php @btn_control() ?></nav>
<article class="cl pd-10">
<div class="col-md-12 box">
<form action="#" method="post" enctype="multipart/form-data">
	<div class="col-md-2">
		<div class="dropbox" style="text-align: center!important;">
			<input type="file" name="logo_image" onchange="readURL(this);" id="file-5" class="hidden inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple  style="display: none!important"/>
			<label for="file-5" style="text-align: center!important;"><img src="img/<?php if($call['logo_image']){echo $call['logo_image'];}else{echo 'img.png';}?>" alt="" id="load"></label><br>
			<span>ເລືອກໂລໂກ້</span>
		</div>
	</div>
	<div class="col-md-10">
		<label>ຊື່(ລາວ) <?php @val() ?></label>
		<input type="text" class="input-text" value="<?php echo $call['name_l'];?>" name="name_l" required>
		<label>ຊື່(ອັງກິດ) <?php @val() ?></label>
		<input type="text" class="input-text" name="name_e" value="<?php echo $call['name_e'];?>" required>
		<label>ຂໍ້ມູນຕິດຕໍ່ <?php @val() ?></label>
		<textarea name="contact" class="textarea" value="<?php echo $call['contact'];?>" required><?php echo $call['contact'];?></textarea>
		<label>ທີ່ຢູ່ <?php @val() ?></label>
		<textarea name="address" class="textarea" value="<?php echo $call['address'];?>" required><?php echo $call['address'];?></textarea><br>
		<input type="checkbox"  <?php if($call['status']=='on'){echo "checked";}else{echo "";} ?> name="status" value="on" required> ໃຊ້ງານ <?php @val() ?><br><br>
		<button <?php echo $permis3; ?> type="submit" name="edit" class="btn btn-primary"><i class="fa fa-check"></i> ຢືນຢັນ</button>
		<button <?php echo $permis3; ?> type="reset" class="btn btn-danger"><i class="fa fa-times"></i> ຍົກເລີກ</button>
	</div>
</form>
</div>
<div class="col-md-12 box">
	<table class="table table-border table-bg table-sort" id="sampleTable2">
		<thead>
			<tr>
				<th>#</th>
				<th>ໂລໂກ້</th>
				<th>ຊື່(ລາວ)</th>
				<th>ຊື່(ອັງກິດ)</th>
				<th>ຂໍ້ມູນຕິດຕໍ່</th>
				<th>ຂໍ້ມູນທີ່ຢູ່</th>
				<th>ສະຖານະ</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$i=1;
				$_Company=$sql($con,"SELECT*FROM qs_company");
				while($res=$array($_Company)){
			 ?>
			<tr>
				<td><?php echo $i ?></td>
				<td><img src="img/<?php if($res['logo_image']){echo $res['logo_image'];}else{echo 'img.png';} ?>" data-darkbox='img/<?php echo $res['logo_image'] ?>' id='load' data-darkbox-group='two' style="width: 100px;height: 100px;border-radius: 50%;border:5px double #ccc"></td>
				<td><?php echo $res['name_l'] ?></td>
				<td><?php echo $res['name_e'] ?></td>
				<td>
                  <input type="checkbox" name="status" <?php if($res['status']=="on"){echo "checked";}else{echo "";}?> disabled="true"> ກຳລັງໃຊ້ງານ</td>
				<td><?php echo $res['contact'] ?></td>
				<td><?php echo $res['address'] ?></td>
				</td>
				<td>
					<div class="btn-group right">
						<a href="?<?php echo $uri;?>&EditCompany&id=<?php echo $res[id] ?>"  class="btn btn-default"><i class="fa fa-pencil"></i></a>
						<a href="#" class="btn btn-default" <?php if($res['status']=="on"){echo "id=disabled";}else{echo "";}?> onclick="delCompany('<?php echo $res[id] ?>')"><i class="fa fa-times"></i></a>
					</div>
				</td>
			</tr>
			<?php $i++;} ?>
		</tbody>
	</table>
</div>
</article>
<?php 
 error_reporting( ~E_NOTICE );
    if(isset($_GET['id']) && !empty($_GET['id']))
    {
        $id = $_GET['id'];
        $stmt_edit = $DB_con->prepare('SELECT * FROM qs_company where id=:id');
        $stmt_edit->execute(array(':id'=>$id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);
    }
    else
    {
        @header("Location:EditCompany.php");
    }
	if(isset($_POST['edit'])){
		$name_l=$Setstring($con,$_POST['name_l']);
		$name_e=$Setstring($con,$_POST['name_e']);
		$contact=$Setstring($con,$_POST['contact']);
		$address=$Setstring($con,$_POST['address']);
		$status=$Setstring($con,$_POST['status']);
		if($status!=""){
		$stt="on";
		}else{
		$stt="off";
		}
		@$file_img =$_FILES['logo_image']['name'];
		@$tmp_dir =$_FILES['logo_image']['tmp_name'];

        if($file_img)
        {
            $upload_dir = 'img/'; // upload directory    
            $fileExt = strtolower(pathinfo($file_img,PATHINFO_EXTENSION)); // get image extension
            $logo_image = rand(1000,1000000).".".$fileExt;        
        }else {
            // if no image selected the old image remain as it is.
            $logo_image = $edit_row['logo_image']; // old image from database
        } 
        $AddCompany=$sql($con,"UPDATE qs_company set name_l='$name_l',name_e='$name_e',logo_image='$logo_image',contact='$contact',address='$address',status='$stt',createdBy='$account_name',createdAt='$timestam' where id='$_GET[id]'");
        $sql($con,"UPDATE qs_company set status='off' where id!='$_GET[id]'");
        if($AddCompany){echo $Success;@move_uploaded_file($tmp_dir,$upload_dir.$logo_image);}else{echo $Fail;}
    }
 ?>
 <?php 
    if(isset($_GET['delCp'])){
        $getImage=$sql($con,"SELECT * FROM qs_company WHERE id='$_GET[delCp]'");
        while ($rows = $array($getImage))
        {
        $del_Company=$sql($con,"DELETE FROM qs_company WHERE id='$_GET[delCp]'");
        if ($del_Company) {
        echo $success; 
        @unlink('img/'.$rows['logo_image']);
        }else{
        echo $fail;
        }  
    }
    }

 ?>
<?php } ?>