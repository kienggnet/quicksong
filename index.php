<!DOCTYPE html>
<?php include 'config/access.php' ?>
<?php include 'Router.php'; ?>
<html>
<head>
	<?php @package() ?>
	<meta charset="utf-8">
	<link rel="Icon" href="img/<?php echo $set_df['logo_image'];?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body>
<?php @navbar() ?>
<?php @menu() ?>
<section class="Hui-article-box" style="overflow-y: auto">
	<?php @mainApp() ?>
</section>
<?php @script() ?>
</body>
</html>
