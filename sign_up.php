<!DOCTYPE html>
<?php include 'app/app.php'; ?>
<?php include 'config/_configdb.php' ?>
<?php include 'app/controller.php'; ?>
<?php include 'app/notification.php'; ?>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php @package() ?>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<style>
		.header{
			padding: 10px;
			margin-bottom: 1%;
			height: 8vh;
			background-color: #FF6600;
			position: relative
			-webkit-box-shadow: 2px 8px 15px -8px rgba(133,129,133,0.66);
			-moz-box-shadow: 2px 8px 15px -8px rgba(133,129,133,0.66);
			box-shadow: 2px 8px 15px -8px rgba(133,129,133,0.66);
		}
		.btn{
			padding: 5px!important;
			width:140px!important;
			border-radius: 3px!important;
			-webkit-box-shadow: 2px 8px 15px -8px rgba(133,129,133,0.66);
			-moz-box-shadow: 2px 8px 15px -8px rgba(133,129,133,0.66);
			box-shadow: 2px 8px 15px -8px rgba(133,129,133,0.66);
		}
	</style>
</head>
<body>
	<div class="header">
		<h3 style='display: inline-flex;margin-top: -15px;color: white;font-weight: bold;'><i class="fa fa-user-plus"></i>&nbsp; ລົງທະບຽນສະມາຊິກ</h3>
		<a href="logout?<?php echo $uri;?>" class="btn btn-success" style="float: right;"><i class="fa fa-sign-in"></i> ລ໋ອກອິນ</a>
	</div>
	<div class="col-md-12">
		<form action="#" method="post" enctype="multipart/form-data">
			<div class="row">
				<marquee behavior="altinate" direction=""></marquee>
				<div class="col-md-8">
					<marquee behavior="altinate"><h3 style="font-weight: bold;color: #FF6600">ເພີ່ມຄວາມສະດວກ ແລະ ວ່ອງໄວ ທຸກການບໍລິຫານງານ</h3></marquee>
					<img src="img/agora_animation.gif" alt="" style="width:100%;display: block;">
				</div>
				<div class="col-md-4">
					<div class="dropbox" style="text-align: center!important;">
						<input type="file" name="user_img" onchange="readURL(this);" id="file-5" class="hidden inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple  style="display: none!important"/>
						<label for="file-5" style="text-align: center!important;"><img src="img/user_null.png" alt="" id="load"></label><br>
						<span>ເລືອກຮູບ</span>
					</div>
					<label>ຊື່ <?php @val(); ?></label>
					<input class="input-text" name="user_fname" required type="text" placeholder="ປ້ອນຊື່">
					<label>ນາມສະກຸນ <?php @val(); ?></label>
					<input class="input-text" name="user_lname" required type="text" placeholder="ປ້ອນນາມສະກຸນ"><br>
					<label style="display: block;">ເພດ <?php @val(); ?></label>
					<div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<div class="radio-box">
							<input type="radio" id="sex-2" value="ຍິງ" name="user_gender"> ຍິງ
						</div>
						<div class="radio-box">
							<input type="radio" id="sex-3" value="ຊາຍ" name="user_gender"> ຊາຍ
						</div>
					</div>
					<br>
					<br>
					<label style="display: block">ວັນເດືອນປີເກີດ <?php @val(); ?></label>
					<input class="input-text" name="user_dob" required type="date" value="<?php echo $date;?>">
					<label>ເລກບັດປະຈຳຕົວ/ສຳມະໂນຄົວ <?php @val(); ?></label>
					<input class="input-text" name="user_card" required type="text" placeholder="xxxx-xxxx-xxxx">
					<label>ທີ່ຢູ່ <?php @val(); ?></label>
					<textarea class="textarea" name="user_address" placeholder="ປ້ອນທີ່ຢູ່" required></textarea>
					<label>ເບີໂທ <?php @val(); ?></label>
					<input class="input-text" name="user_tel" type="text" placeholder="+856 xxxxxxxx">
					<label>ອີເມວ</label>
					<input class="input-text" name="user_email" type="email" placeholder="example@gmail.com">
					<label>ຊື່ເຂົ້າໃຊ້ລະບົບ <?php @val(); ?></label>
					<input class="input-text" name="user_name" required type="text" minlength="3" placeholder="ປ້ອນຊື່ເຂົ້າໃຊ້ລະບົບ">
					<label>ລະຫັດສ່ວນຕົວ <?php @val(); ?></label>
					<input class="input-text" name="user_password"  minlength="6" maxlength="12" required type="password" placeholder="ປ້ອນລະຫັດຜ່ານ">
					<br><br>
					<button type="submit" name="add_user" class="btn btn-primary"><i class="fa fa-save"></i> ບັນທຶກ</button>&nbsp;&nbsp;
					<button type="reset"  class="btn btn-danger"><i class="fa fa-times"></i> ຍົກເລີກ</button>
					<div style="height: 20px!important"></div>
				</div>
			</form>
		</div>
	</div>
	<?php @Script() ?>
</body>
</html>

<?php 
error_reporting( ~E_NOTICE );
if(isset($_POST['add_user'])){
	@$user_id=$Setstring($con,$_POST['user_id']);
	@$user_fname=$Setstring($con,$_POST['user_fname']);
	@$user_lname=$Setstring($con,$_POST['user_lname']);
	@$user_gender=$Setstring($con,$_POST['user_gender']);
	@$user_dob=$Setstring($con,$_POST['user_dob']);
	@$user_card=$Setstring($con,$_POST['user_card']);
	@$user_address=$Setstring($con,$_POST['user_address']);
	@$user_tel=$Setstring($con,$_POST['user_tel']);
	@$user_email=$Setstring($con,$_POST['user_email']);
	@$user_name=$Setstring($con,$_POST['user_name']);
	@$user_password=$Setstring($con,md5($_POST['user_password']));

	@$file_img = $_FILES['user_img']['name'];
	@$file_type = $_FILES['user_img']['type'];
	@$tmp_dir = $_FILES['user_img']['tmp_name'];
	@$fileSize = $_FILES['user_img']['size'];
@$upload_dir = 'img/'; // upload directory
@$fileExt = strtolower(pathinfo($file_img,PATHINFO_EXTENSION));
@$img = rand(100000,1000000).".".$fileExt;

$add_user=$sql($con,"INSERT INTO qs_users values('$NewId','$user_fname','$user_lname','$user_gender','$user_dob','$user_card','$user_address','$user_tel','$user_email','$user_name','$user_password','Users','$img','$timestam','true')");
if($add_user){echo $Success;@move_uploaded_file($tmp_dir,$upload_dir.$img);}else{echo $Fail;}
}
?>