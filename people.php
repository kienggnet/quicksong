﻿
<?php function _people(){ ?>
<?php include 'app/notification.php'; ?>
<nav class="breadcrumb">
<i class="fa fa-users"></i> 
<span class="c-666"> ສະມາຊິກ</span><?php @btn_control() ?></nav>
<article class="cl pd-10">
<div class="col-md-6">
	<h3><i class="fa fa-users"></i> ສະມາຊິກກຳລັງອອນໄລນ໌</h3>
	<div class="box" id="displays" style="overflow-x: scroll;max-height: 90vh">
	</div>
</div>
<div class="col-md-6">
	<h3><i class="fa fa-users" style="color:#ccc"></i> ສະມາຊິກທັງໝົດ</h3>
	<div class="box" style="overflow-x: scroll;max-height: 90vh">
		<?php 
		include 'config/_configdb.php';
		$newData=$sql($con,"SELECT*FROM qs_customer where status='off' or status='block' order by createdAt  desc");
		while($item=$array($newData)){
			?>
			<div class="list-song">
				<div class="col-md-2" 
				style="
				display: inline-block;
				width: 20%;
				text-align: center;
				border-right: 2px solid #ccc">
				<h1><i class="fa fa-user" style="color:#ccc"></i></h1>
			</div>
			<div class="col-md-6 wrap-text">
				<p class="wrap-text"><b>ຊື່ສະມາຊິກ:</b> <?php echo $item['username'] ?> </i></p>
				<p class="wrap-text"><b>ວັນທີລົງທະບຽນ:</b> <?php echo $item['createdAt'] ?></p>
			</div>
			<div class="col-md-2" style="float: right;cursor: pointer;margin-right: -5%!important"  onclick="Remove(<?php echo $item['id'] ?>)"> 
				<i class="fa fa-trash fa-3x" style="color:red!important"></i>
				</div>
			</div>
			<?php
		}
		?>
	</div>
	<?php 
		if(isset($_GET['del'])){
			$change=$sql($con,"UPDATE qs_customer set status='off' where id='$_GET[del]'");
			if($change){echo $success;}else{echo $fail;}
		}
		if(isset($_GET['block'])){
			$change=$sql($con,"UPDATE qs_customer set status='block' where id='$_GET[block]'");
			if($change){echo $success;}else{echo $fail;}
		}
		if(isset($_GET['move'])){
			$change=$sql($con,"DELETE FROM qs_customer where id='$_GET[move]'");
			if($change){echo $success;}else{echo $fail;}
		}
	 ?>
</div>
</article>
<?php 	} ?>

	