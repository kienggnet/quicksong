<?php function _EditMember(){ ?>
<?php include 'config/_configdb.php' ?>
<?php include 'app/notification.php'; ?>
  <?php 
  $data_user=$sql($con,"SELECT*FROM qs_users where user_id='$_GET[page]'");
  $_res=$array($data_user);
 ?>
<nav class="breadcrumb"><a href="?/settings&/<?php echo $uri;?>"><i class="fa fa-cog"></i> <span class="c-666">ຕັ້ງຄ່າ </a> / <a href="?/AllMember&/<?php echo $uri;?>"> ຜູ້ໃຊ້ລະບົບ</a> / ແກ້ໄຂ</span><?php @btn_control() ?></nav>
<article class="cl pd-10">
	<div class="col-md-12 box">
		<form action="#" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="dropbox" style="text-align: center!important;">
						<input type="file" name="user_img" onchange="readURL(this);" id="file-5" class="hidden inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple  style="display: none!important"/>
						<label for="file-5" style="text-align: center!important;"><img src="img/<?php if($_res['user_img']){echo $_res['user_img'];}else{echo "user_null.png";}?>" alt="" id="load" ></label><br>
						<span>ເລືອກຮູບ</span>
					</div>
					 <label>ຊື່ <?php @val(); ?></label>
            <input class="input-text" name="user_fname" value="<?php echo $_res['user_fname'];?>" required type="text">
            <label>ນາມສະກຸນ <?php @val(); ?></label>
            <input class="input-text" name="user_lname" value="<?php echo $_res['user_lname'];?>" required type="text">
            <label>ເພດ <?php @val(); ?></label>
            <div class="formControls col-xs-8 col-sm-9 skin-minimal">
						<div class="radio-box">
							<input type="radio" id="sex-2" value="ຍິງ" <?php if($_res['user_gender']=="ຍິງ"){echo "checked";}else{echo "";} ?> name="user_gender"> ຍິງ
						</div>
						<div class="radio-box">
							<input type="radio" id="sex-3" value="ຊາຍ" <?php if($_res['user_gender']=="ຊາຍ"){echo "checked";}else{echo "";}?> name="user_gender"> ຊາຍ
						</div>
					</div><br>
                  <label>ວັນເດືອນປີເກີດ <?php @val(); ?></label>
                  <input class="input-text" name="user_dob" value="<?php echo $_res['user_dob'];?>" required type="date">
                  <label>ເລກບັດປະຈຳຕົວ/ສຳມະໂນຄົວ <?php @val(); ?></label>
                  <input class="input-text" name="user_card" value="<?php echo $_res['user_card'];?>" required  type="text">
                  <label>ທີ່ຢູ່ <?php @val(); ?></label>
                  <textarea class="textarea" name="user_address" value="<?php echo $_res['user_address'];?>" required><?php echo $_res['user_address'];?></textarea>
                  <label>ເບີໂທ <?php @val(); ?></label>
                  <input class="input-text" name="user_tel" value="<?php echo $_res['user_tel'];?>" required type="text">
                  <label>ອີເມວ</label>
                  <input class="input-text" name="user_email" value="<?php echo $_res['user_email'];?>" type="email">
                  <label>ຊື່ເຂົ້າໃຊ້ລະບົບ <?php @val(); ?></label>
                  <input class="input-text" name="user_name" value="<?php echo $_res['user_name'];?>" required type="text">
                  <label <?php echo $permis ?>>ສິດທິການໃຊ້ງານ <?php @val(); ?></label>
                  <select <?php echo $permis ?> name="user_status" class="input-text" id="">
                    <option value=""><?php echo $_res['user_status'];?></option>
                    <option value="Admin">Admin</option>
                    <option value="Users">Users</option>
                  </select>
                  <br><br>
					<button type="submit" name="edit" class="btn btn-success"><i class="fa fa-check"></i> ຢືນຢັນ</button>&nbsp;&nbsp;
					<button type="reset"  class="btn btn-danger"><i class="fa fa-times"></i> ຍົກເລີກ</button>
				</div>
			</form>
		</div>
		<div class="col-md-3"></div>
	</div>
</article>
<?php  
    error_reporting( ~E_NOTICE );
    if(isset($_GET['page']) && !empty($_GET['page']))
    {
        $user_id = $_GET['page'];
        $stmt_edit = $DB_con->prepare('SELECT * FROM qs_users where user_id=:user_id');
        $stmt_edit->execute(array(':user_id'=>$user_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);
    }
    else
    {
        @header("Location:EditMember.php");
    }
    if(isset($_POST['edit'])){
    @$user_fname=$Setstring($con,$_POST['user_fname']);
    @$user_lname=$Setstring($con,$_POST['user_lname']);
    @$user_gender=$Setstring($con,$_POST['user_gender']);
    @$user_dob=$Setstring($con,$_POST['user_dob']);
    @$user_card=$Setstring($con,$_POST['user_card']);
    @$user_address=$Setstring($con,$_POST['user_address']);
    @$user_tel=$Setstring($con,$_POST['user_tel']);
    @$user_email=$Setstring($con,$_POST['user_email']);
    @$user_name=$Setstring($con,$_POST['user_name']);


    @$file_img = $_FILES['user_img']['name'];
    @$file_type = $_FILES['user_img']['type'];
    @$tmp_dir = $_FILES['user_img']['tmp_name'];
    @$fileSize = $_FILES['user_img']['size'];
        if($file_img)
        {
            $upload_dir = 'img/'; // upload directory    
            $us_img = strtolower(pathinfo($file_img,PATHINFO_EXTENSION)); // get image extension
            $new_img = rand(1000,1000000).".".$us_img;        
        }else {
            // if no image selected the old image remain as it is.
            $new_img = $edit_row['user_img']; // old image from database
        }  
        $function_edit=$sql($con,"UPDATE qs_users SET user_fname='$user_fname',user_lname='$user_lname',user_gender='$user_gender',user_dob='$user_dob',user_card='$user_card',user_address='$user_address',user_tel='$user_tel',user_email='$user_email',user_name='$user_name',user_img='$new_img',sign_date='$timestam' where user_id='$_GET[page]'");

        if($function_edit){
            echo $Success;
            @unlink($upload_dir.$edit_row['user_img']);
            @move_uploaded_file($tmp_dir,$upload_dir.$new_img);   
        }else{ 
          echo $Fail;
        }
    }
  ?> 
    <?php 	} ?>