<!DOCTYPE html>
<html>
<head>
<?php include '../config/_configdb.php' ?>
<?php 
	$get_user=$sql($con,"SELECT *FROM qs_users where user_id='$_GET[id]'");
	$res=$assoc($get_user);
 ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../font/font-style.css">
<link rel="stylesheet" href="../css/app.css">
<link rel="stylesheet" type="text/css" href="../lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="../static/h-ui.admin/css/style.css" />
<style>
	td{
		padding: 0.5px!important;
	}
</style>
</head>
<body>
<div class="col-md-12">
<table width="100%" style="margin-bottom: 2%!important">
	<tr>
		<td colspan="2" style="width: 130px;vertical-align: top!important;background-image:url('../img/b95608_dd67f0be26b947dc9868fb96c0ffba64_mv2.gif');background-position: center;background-size: cover;">
			<img src="../img/<?php if($res['user_img']){echo $res['user_img'];}else{echo 'user_null.png';} ?>" style="width: 120px;height: 120px;margin: 10px;border-radius: 50%;border:5px double #f1f1f1">
		</td>
	</tr>
	<tr><td colspan="2" class="headerDetail" style="padding: 10px!important">ລາຍລະອຽດ</td></tr>
	<tr>
		<td style="padding: 10px">
			<table>
				<tr>
					<td style="width: 50%"><label for="">ລະຫັດ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_id'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ວັນເດືອນປີເກີດ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_dob'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ເພດ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_gender'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ທີ່ຢູ່: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_address'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ເລກບັດປະຈຳຕົວ/ສຳມະໂນຄົວ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_card'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ເບີໂທ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_tel'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ອີເມວ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_email'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">UserName: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_name'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ສະຖານະ: </label></td>
					<td  style="width: 50%"> <?php echo $res['user_status'] ?></td>
				</tr>
				<tr>
					<td style="width: 50%"><label for="">ວັນທີລົງທະບຽນ: </label></td>
					<td  style="width: 50%"> <?php echo $res['sign_date'] ?></td>
				</tr>
			</table>
		</tr>
	</td>
</table>
</div>
<script type="text/javascript" src="../lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="../static/h-ui/js/H-ui.js"></script> 
<script type="text/javascript" src="../static/h-ui.admin/js/H-ui.admin.page.js"></script> 

</body>
</html>