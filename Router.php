<?php include 'config/_configdb.php';  ?>
<?php include 'Company/setDefault.php' ?>
<title><?php echo $set_df['name_l'].' / '.$set_df['name_e']; ?></title>
<link rel="Icon" href="img/<?php echo $set_df['logo_image'];?>">
<?php 
// path
require('app/app.php');
include 'config/access.php';
include 'config/_configdb.php';
include 'app/notification.php';
include 'app/controller.php';
include 'setting.php';
include 'bank.php';
include 'Company/ListCompany.php';
include 'Company/EditCompany.php';
include 'addOrder/index.php';
include 'people.php';
include 'history.php';
// Router
function mainApp(){
	if(isset($_GET['/home'])){
		@_home();
	}
	if(isset($_GET['/settings'])){
		@settings();
	}
	if(isset($_GET['/index'])){
		@_home();
	}
	if(isset($_GET['/company'])){
		@_company();
	}
	if(isset($_GET['EditCompany'])){
		@_Editcompany();
	}
	if(isset($_GET['/addOrder'])){
		@_AddOrders();
	}
	if(isset($_GET['/peopleOnline'])){
		@_people();
	}
	if(isset($_GET['/history'])){
		@_history();
	}
}
?>