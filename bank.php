﻿
<?php function _home(){ ?>
<?php include 'app/notification.php'; ?>
<nav class="breadcrumb">
<i class="fa fa-home"></i> 
<span class="c-666"> ໜ້າຫຼັກ</span><?php @btn_control() ?></nav>
<article class="cl pd-10">
<div class="col-md-6">
	<h3><i class="fa fa-music"></i> ລາຍການເພງທີ່ກຳລັງຂໍ</h3>
	<div class="box" id="display" style="overflow-x: scroll;max-height: 90vh">
	</div>
</div>
<div class="col-md-6">
	<h3><i class="fa fa-music"></i> ລາຍການເພງທີ່ເປີດແລ້ວ</h3>
	<div class="box" style="overflow-x: scroll;max-height: 90vh">
		<?php 
		include 'config/_configdb.php';
		$newData=$sql($con,"SELECT*FROM qs_order_song where status='off' and updatedAt='$date' order by updatedAt  desc");
		while($item=$array($newData)){
			?>
			<div class="list-song">
				<div class="col-md-2" 
				style="
				display: inline-block;
				width: 20%;
				text-align: center;
				border-right: 2px solid #ccc">
				<h1><b>0<?php echo $item['tb_number'] ?></b></h1>
			</div>
			<div class="col-md-6 wrap-text">
				<p class="wrap-text"><b>ຊື່ເພງ:</b> <?php echo $item['title'] ?> <i class="fa fa-music"></i></p>
				<p class="wrap-text"><b>ພິເສດ:</b> <?php echo $item['detail'] ?></p>
			</div>
			<div class="col-md-2" style="float: right;"> 
				<i class="fa fa-microphone-slash fa-3x" style="color:red!important"></i>
				</div>
			</div>
			<?php
		}
		?>
	</div>
	<?php 
		if(isset($_GET['del'])){
			$change=$sql($con,"UPDATE qs_order_song set status='off',updatedAt='$timestam',updatedBy='$account_fname' where Id='$_GET[del]'");
			if($change){echo $success;}else{echo $fail;}
		}
	 ?>
</div>
</article>
<?php 	} ?>

	